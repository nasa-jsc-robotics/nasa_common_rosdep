Change Log
==========

2.0.1
-----

* Fixed the name of the nasa_common_log4cpp debian package

2.0.0
-----

* Updated nasa-common-dep to reflect er4 debian naming convention for 20.04
* Added pyqt_qml to nasa-pyqt5.yaml

1.9.0
-----

* Updated python3 version of packages used by taskforce: python-pyqtgraph, python-jsonschema, python-zmq, python-yappi, python-qscintilla2, pyqt-dev-tools. Made the switch to use apt over pip for these packages if possible.
* Added rosbag_er4_utils
* Added python-rosbag-pandas and python-bokeh definitions
* Added python-xmlschema definition
* Added `terminator` and `selectors2`
* Added `log4cpp` to `trusty` to point to the ROS distribution directly, since `rosdep` can't figure it out.
* Updated maintainers in `README.md`

1.8.0
-----

* Added raven-drivers definition
* Added tmux, libtmux, and termcolor definitions to support ROStep++

1.7.1
-----

* Added pulseaudio definition

1.7.0
-----

* Changed the name of qscintilla2 package and made it os specific
* Added python_qml for both OS
* Corrected ntplib package name

1.6.0
-----

* Removed non existent package.
* Added `pyqt-dev-tools` disambiguation in `nasa-pyqt[4/5].yaml`

1.5.3
-----

* Added python-yappi definition

1.5.2
-----

* Added python-pycurl definition

1.5.1
-----

* Removed `gazebo` keys that correspond to `gazebo5` and `gazebo7`, as they are now upstream.

1.5.0
-----

* Removed `.*graphviz.*` from list as it exists upstream: [https://github.com/ros/rosdistro/blob/master/rosdep/base.yaml#L1009](https://github.com/ros/rosdistro/blob/master/rosdep/base.yaml#L1009)
* Added gazebo6 and gazebo7 definitions
* Added Xenial definitions
